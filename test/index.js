const factory = require('./factory')

if (process.env.NODE_ENV === 'test') {
    describe('User tests\n  ____________________________________________________________', function() {

        beforeEach('', async function() {
            await factory.adminModel.save()
        })

        after('', async function() {
            await factory.adminModel.remove()
        })

        describe('DELETE \'/api/users/:uid\'', function() {
            require('./users')
        })

        describe('POST \'/api/users/sign-up\'', function() {
            require('./users-signup')
        })

        describe('POST \'/api/users/sign-in\'', function() {
            require('./users-signin')
        })

    })

    describe.skip('Post tests\n  ____________________________________________________________', function() {
        describe('POST \'/api/posts\'', function() {
            require('./posts')
        })
    })
}
