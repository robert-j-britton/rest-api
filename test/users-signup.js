const chai = require('chai')
const chaiHTTP = require('chai-http')
const User = require('../models/User')
const app = require('../index')
const factory = require('./factory')

const should = chai.should();
chai.use(chaiHTTP)

const signUpURI = '/api/users/sign-up'

const generateString = (length = 10) => {
    return [...Array(length)].map(i=>(~~(Math.random()*36)).toString(36)).join('')
}

describe('test user object\'s email property', function() {

    it("it should fail when email property is an empty string", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({ ...factory.validAuthor, email: '' })
        .end(function(err, res) {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error').eql('email is not allowed to be empty')
            done()
        })
    })

    it("it should fail when email is undefined", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({ ...factory.validAuthor, email: undefined })
        .end(function(err, res) {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error').eql('email is required')
            done()
        })
    })

    it("it should fail when email is a number", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({ ...factory.validAuthor, email: 1234 })
        .end(function(err, res) {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error').eql('email must be a string')
            done()
        })
    })

    it("it should fail when email is malformed", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({ ...factory.validAuthor, email: 'test.com' })
        .end(function(err, res) {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error').eql('email must be a valid email')
            done()
        })
    })

    it("it should fail when email is an object", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({ ...factory.validAuthor, email: { illegalProp: 1234 } })
        .end(function(err, res) {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error').eql('email must be a string')
            done()
        })
    })

    it("it should fail when email localdomain (<localdomain>@test.com) length === 65", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({
            ...factory.validAuthor,
            email: `${generateString(65)}@test.com`
        })
        .end(function(err, res) {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error').eql('email must be a valid email')
            done()
        })
    })

    it("it should fail when email property remote domain (test@<remotedomain>.com) length === 64", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({
            ...factory.validAuthor,
            email: `test@${generateString(64)}.com`
        })
        .end(function(err, res) {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error').eql('email must be a valid email')
            done()
        })
    })

    it("it should fail when email is taken", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send(factory.validAdmin)
        .end(function(err, res) {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error').eql('email already registered')
            done()
        })
    })

    it("it should fail when email property length === 4", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({ ...factory.validAdmin, email: 'r@.e' })
        .end(function(err, res) {
            res.should.have.status(400)
            res.body.error.should.be.equal('email length must be at least 5 characters long')
            done()
        })
    })

    it("it should pass when email property local domain length === 64 and remote domain length === 63 (<localdomain>@<remotedomain>.com) ", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({
            ...factory.validAuthor,
            email: `${generateString(64)}@${generateString(63)}.com`
        })
        .end(function(err, res) {
            res.should.have.status(200)
            // Remove the user then end test
            User.findOne({ email: res.body.data.email }).exec(async function(err, res) {
                await res.remove()
                done()
            })
        })
    })

    it("it should pass when email length === 5", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({ ...factory.validAuthor, email: 'a@b.c'})
        .end(function(err, res) {
            res.should.have.status(200)
            res.body.should.be.a('object')
            User.findOne({ email: res.body.data.email }).exec(async function(err, res) {
                await res.remove()
                done()
            })
        })
    })
})

describe("test user object\'s password property", function() {

    it("it should fail with 400 when the password is undefined", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({ ...factory.validAuthor, password: undefined })
        .end(function(err, res) {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error').eql('password is required')
            done()
        })
    })

    it("it should fail with 400 when password is an empty string", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({ ...factory.validAuthor, password: '' })
        .end(function(err, res) {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error').eql('password is not allowed to be empty')
            done()
        })
    })

    it("it should fail with 400 when password length is < 5", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({ ...factory.validAuthor, password: '1' })
        .end(function(err, res) {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error').eql('password length must be at least 6 characters long')
            done()
        })
    })

    it("it should fail with 400 when password length is > 70", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({ ...factory.validAuthor, password: generateString(71) })
        .end(function(err, res) {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error').eql('password length must be less than or equal to 70 characters long')
            done()
        })
    })

    it("it should fail with 400 when email length is === 4", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({ ...factory.validAuthor, password: generateString(4) })
        .end(function(err, res) {
            res.should.have.status(400)
            res.body.should.be.a('object')
            done()
        })
    })

    it("it should fail with 400 when password is a number", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({ ...factory.validAuthor, password: 1234 })
        .end(function(err, res) {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error').eql('password must be a string')
            done()
        })
    })

    it("it should fail with 400 when password is an object", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({ ...factory.validAuthor, password: { illegalProp: 1234 } })
        .end(function(err, res) {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error').eql('password must be a string')
            done()
        })
    })

    it("it should pass when password length is > 5 < 71", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({ ...factory.validAuthor, password: generateString(50) })
        .end(function(err, res) {
            res.should.have.status(200)
            res.body.should.be.a('object')
            User.findOne({ email: res.body.data.email }).exec(async function(err, res) {
                await res.remove()
                done()
            })
        })
    })

    it("it should pass when password length is === 70", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({ ...factory.validAuthor, password: generateString(70) })
        .end(function(err, res) {
            res.should.have.status(200)
            res.body.should.be.a('object')
            // Remove the user then end test
            User.findOne({ email: res.body.data.email }).exec(async function(err, res) {
                await res.remove()
                done()
            })
        })
    })
})

describe("test user object\'s name property", function() {

    it("it should fail when name is a number", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({ ...factory.validAuthor, name: 1234 })
        .end(function(err, res) {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error').eql('name must be a string')
            done()
        })
    })

    it("it should fail when name is an empty string", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({ ...factory.validAuthor, name: '' })
        .end(function(err, res) {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error').eql('name is not allowed to be empty')
            done()
        })
    })

    it("it should fail when name is undefined", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({ ...factory.validAuthor, name: undefined })
        .end(function(err, res) {
            res.should.have.status(400)
            res.body.should.be.a('object')
            res.body.should.have.property('error').eql('name is required')
            done()
        })
    })

})

describe("test user object", function() {
    it("it should fail when the user object is missing", function(done) {
        chai.request(app)
        .post(signUpURI)
        .end(function(err, res) {
            res.should.have.status(400)
            done()
        })
    })

    it("it should fail when an empty object is passed", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send({})
        .end(function(err, res) {
            res.should.have.status(400)
            done()
        })
    })

    it("it should pass when the user object with valid email, name and password is given", function(done) {
        chai.request(app)
        .post(signUpURI)
        .send(factory.validAuthor)
        .end(function(err, res) {
            res.should.have.status(200)
            res.body.should.be.a('object')
            res.body.should.have.property('data')
            res.body.data.should.have.property('email').eql(factory.validAuthor.email)
            res.body.should.not.have.property('password')
            res.body.should.not.have.property('token')
            User.findOne({ email: res.body.data.email }).exec(async function(err, res) {
                await res.remove()
                done()
            })
        })
    })
})



