const chai = require('chai')
const chaiHTTP = require('chai-http')
const _ = require('lodash')
const User = require('../models/User')
const Post = require('../models/Post')
const app = require('../index')
const Constants = require('../Constants')
const should = chai.should();
chai.use(chaiHTTP)

const postsURI = '/api/posts'
const email = 'test@testing.com'
const name = 'Test User'
const passwordRaw = 'test1234'


// A valid post. Use directly or create a copy, make changes to copy and use to test
const post = {
    exerpt: "This is a quick summary of the post",
    title: "Javascript for idiots",
    tags:["express", "mocha", "chai"],
    body: {"h2": "This is a sub-title"},
    category: "node-js",
    isPublished: true
}

let authToken;


before('Create User', function(done) {
    const user = new User({
        email: email,
        name: name,
        password: passwordRaw
    })
    authToken = user.generateAuthToken()
    user.save(function(err) {
        done()
    })
});

after('Delete User', function(done) {
    User.deleteMany({}, function(err) {
        done()
    })
});

after('Delete All Posts', function(done) {
    Post.deleteMany({}).exec(function(err, res) {
        done()
    })
})

describe('Create a post without client authentication - user not signed in', function() {
    it('should return an object with an error and a http status 401', function(done) {
        chai.request(app)
        .post(postsURI)
        .end(function(err, res) {
            res.body.should.have.property('error')
            res.body.should.not.have.property('data')
            res.should.have.status(401)
            done()
        })
    })
})

describe('Create a valid post with client authenthenticated - user signed in', function() {
    it('should return an object with an error and a http status 200', function(done) {
        chai.request(app)
        .post(postsURI)
        .set('x-auth-token', authToken)
        .send(post)
        .end(function(err, res) {
            res.body.should.have.property('data')
            res.body.should.not.have.property('error')
            res.should.have.status(200)
            done()
        })
    })
})

describe('Create a post with body missing', function() {
    it('should return an object with an error and a http status 400', function(done) {
        const updatedPost = { ...post, body: undefined }

        chai.request(app)
        .post(postsURI)
        .set('x-auth-token', authToken)
        .send(updatedPost)
        .end(function(err, res) {
            res.body.should.have.property('error')
            res.body.should.not.have.property('data')
            res.should.have.status(400)
            done()
        })
    })
})

describe('Create a post with body property as an empty object', function() {
    it('should return an object with an error and a http status 400', function(done) {
        const updatedPost = { ...post, body: {} }

        chai.request(app)
        .post(postsURI)
        .set('x-auth-token', authToken)
        .send(updatedPost)
        .end(function(err, res) {
            res.body.should.have.property('error')
            res.body.should.not.have.property('data')
            res.should.have.status(400)
            done()
        })
    })
})

describe('Create a post with body property as string', function() {
    it('should return an object with an error and a http status 400', function(done) {
        const updatedPost = { ...post, body: "" }

        chai.request(app)
        .post(postsURI)
        .set('x-auth-token', authToken)
        .send(updatedPost)
        .end(function(err, res) {
            res.body.should.have.property('error')
            res.body.should.not.have.property('data')
            res.should.have.status(400)
            done()
        })
    })
})

describe('Create a post with body property as an empty array', function() {
    it('should return an object with an error and a http status 400', function(done) {
        const updatedPost = { ...post, body: [] }

        chai.request(app)
        .post(postsURI)
        .set('x-auth-token', authToken)
        .send(updatedPost)
        .end(function(err, res) {
            res.body.should.have.property('error')
            res.body.should.not.have.property('data')
            res.should.have.status(400)
            done()
        })
    })
})

describe('Create a post with title property as undefined', function() {
    it('should return an object with an error and a http status 400', function(done) {
        const updatedPost = { ...post, title: undefined }

        chai.request(app)
        .post(postsURI)
        .set('x-auth-token', authToken)
        .send(updatedPost)
        .end(function(err, res) {
            res.body.should.have.property('error')
            res.body.should.not.have.property('data')
            res.should.have.status(400)
            done()
        })
    })
})

describe('Create a post with title property as a number', function() {
    it('should return an object with an error and a http status 400', function(done) {
        const updatedPost = { ...post, title: 6546546 }

        chai.request(app)
        .post(postsURI)
        .set('x-auth-token', authToken)
        .send(updatedPost)
        .end(function(err, res) {
            res.body.should.have.property('error')
            res.body.should.not.have.property('data')
            res.should.have.status(400)
            done()
        })
    })
})

describe('Create a post with title property as an empty object', function() {
    it('should return an object with an error and a http status 400', function(done) {
        const updatedPost = { ...post, title: {} }

        chai.request(app)
        .post(postsURI)
        .set('x-auth-token', authToken)
        .send(updatedPost)
        .end(function(err, res) {
            res.body.should.have.property('error')
            res.body.should.not.have.property('data')
            res.should.have.status(400)
            done()
        })
    })
})

describe('Create a post with title property as an obeject with title property', function() {
    it('should return an object with an error and a http status 400', function(done) {
        const updatedPost = { ...post, title: { title: 'A New Title - But Invalid'} }

        chai.request(app)
        .post(postsURI)
        .set('x-auth-token', authToken)
        .send(updatedPost)
        .end(function(err, res) {
            res.body.should.have.property('error')
            res.body.should.not.have.property('data')
            res.should.have.status(400)
            done()
        })
    })
})

describe('Create a post with title property as an obeject with title property', function() {
    it('should return an object with an error and a http status 400', function(done) {
        const updatedPost = { ...post, exerpt: undefined }

        chai.request(app)
        .post(postsURI)
        .set('x-auth-token', authToken)
        .send(updatedPost)
        .end(function(err, res) {
            res.body.should.have.property('error')
            res.body.should.not.have.property('data')
            res.should.have.status(400)
            done()
        })
    })
})


