const User = require('../models/User')

const validAuthor = {
    email: 'test@test.com',
    name: 'Nedd Stark',
    password: 'topsecretpass',
    role: 'author'
}

const validAdmin = {
    email: 'adminuser@test.com',
    name: 'Cersei Lannister',
    password: 'deathbyceilingcollapse...',
    role: 'admin'
}

const validAuthor2 = {
    ...validAuthor,
    email: 'testing@test.com'
}

const authorModel = new User(validAuthor)
const validAuthorToken = authorModel.generateAuthToken()

const author2Model = new User(validAuthor2)
const validAuthor2Token = author2Model.generateAuthToken()

const adminModel = new User(validAdmin)
const validAdminToken = adminModel.generateAuthToken()


module.exports = {
    validAuthor,
    authorModel,
    validAuthorToken,
    validAuthor2,
    author2Model,
    validAuthor2Token,
    validAdmin,
    adminModel,
    validAdminToken
}
