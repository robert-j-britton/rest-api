const chai = require('chai')
const chaiHTTP = require('chai-http')
const app = require('../index')
const Constants = require('../Constants')
const factory = require('./factory')

const should = chai.should()
chai.use(chaiHTTP)

beforeEach('Save an author to the db', function(done) {
    factory.authorModel.save(function() {
        done()
    })
})

after('Remove the author user from the db', function(done) {
    factory.authorModel.remove(function() {
        done()
    })
})

it('should fail when an author tries to delete an author', function(done) {
    chai.request(app)
    .delete(`/api/users/${factory.authorModel._id}`)
    .set('x-auth-token', factory.validAuthor2Token)
    .end(function(err, res) {
        res.should.have.status(403)
        res.body.should.be.a('object')
        res.body.should.have.property('error')
        done()
    })
})

it('should fail when an author attempts to delete an admin user', function(done) {
    chai.request(app)
    .delete(`/api/users/${factory.adminModel._id}`)
    .set('x-auth-token', factory.validAuthorToken)
    .end(function(err, res) {
        res.should.have.status(403)
        res.body.should.be.a('object')
        res.body.should.have.property('error').eql(Constants.ACCESS_DENIED)
        done()
    })
})

it('should fail when an invalid token is passed', function(done) {
    chai.request(app)
    .delete(`/api/users/${factory.authorModel._id}`)
    .set('x-auth-token', 'invalidtoken')
    .end(function(err, res) {
        res.should.have.status(400)
        res.body.should.be.a('object')
        res.body.should.have.property('error')
        done()
    })
})

it('should fail when an invalid user id is passed', function(done) {
    chai.request(app)
    .delete(`/api/users/${factory.authorModel._id}invalid`)
    .set('x-auth-token', factory.validAdminToken)
    .end(function(err, res) {
        res.should.have.status(404)
        res.body.should.be.a('object')
        res.body.should.have.property('error')
        done()
    })
})

it('should fail when no token is passed', function(done) {
    chai.request(app)
    .delete(`/api/users/${factory.authorModel._id}`)
    .end(function(err, res) {
        res.should.have.status(401)
        res.body.should.be.a('object')
        res.body.should.have.property('error')
        done()
    })
})

it('should fail when no user id is passed', function(done) {
    chai.request(app)
    .delete(`/api/users`)
    .end(function(err, res) {
        res.should.have.status(404)
        done()
    })
})

it('should pass when an admin user deletes an author', function(done) {
    chai.request(app)
    .delete(`/api/users/${factory.authorModel._id}`)
    .set('x-auth-token', factory.validAdminToken)
    .end(function(err, res) {
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('data')
        res.body.data.should.have.property('_id').eql(factory.authorModel._id.toString())
        done()
    })
})

it('should pass when an author deletes himself', function(done) {
    chai.request(app)
    .delete(`/api/users/${factory.authorModel._id}`)
    .set('x-auth-token', factory.validAuthorToken)
    .end(function(err, res) {
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('data')
        res.body.data.should.have.property('_id').eql(factory.authorModel._id.toString())
        done()
    })
})

it('should pass when an admin user deletes himself', function(done) {
    chai.request(app)
    .delete(`/api/users/${factory.adminModel._id}`)
    .set('x-auth-token', factory.validAdminToken)
    .end(function(err, res) {
        res.should.have.status(200)
        res.body.should.be.a('object')
        res.body.should.have.property('data')
        res.body.data.should.have.property('_id').eql(factory.adminModel._id.toString())
        done()
    })
})
