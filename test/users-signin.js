const chai = require('chai')
const chaiHTTP = require('chai-http')
const app = require('../index')
const Constants = require('../Constants')
const factory = require('./factory')

const should = chai.should()
chai.use(chaiHTTP)

const signInURI = '/api/users/sign-in'

it("it should fail when no params are sent", function(done) {
    chai.request(app)
    .post(signInURI)
    .end(function(err, res) {
        res.should.have.status(400)
        res.body.should.be.a('object')
        res.body.should.have.property('error').eql('email is required')
        done()
    })
})

it('it should fail when the email does not exist on db', function(done) {
    chai.request(app)
    .post(signInURI)
    .send({ ...factory.validAdmin, email: 'test@doesntexist.com' })
    .end(function(err, res) {
        res.should.have.status(400)
        res.body.should.be.a('object')
        res.body.should.have.property('error').eql(Constants.INVALID_EMAIL_PASSWORD)
        done()
    })
})

it('it should fail when the email is malformed', function(done) {
    chai.request(app)
    .post(signInURI)
    .send({ ...factory.validAdmin,
        email: 'testing.com',
    })
    .end(function(err, res) {
        res.should.have.status(400)
        res.body.should.be.a('object')
        res.body.should.have.property('error').eql('email must be a valid email')
        done()
    })
})

it('it should fail when the email is an empty string', function(done) {
    chai.request(app)
    .post(signInURI)
    .send({ ...factory.validAdmin, email: '' })
    .end(function(err, res) {
        res.should.have.status(400)
        res.body.should.be.a('object')
        res.body.should.have.property('error').eql('email is not allowed to be empty')
        done()
    })
})

it('it should fail when the email is undefined', function(done) {
    chai.request(app)
    .post(signInURI)
    .send({ ...factory.validAdmin, email: undefined })
    .end(function(err, res) {
        res.should.have.status(400)
        res.body.should.be.a('object')
        res.body.should.have.property('error').eql('email is required')
        done()
    })
})

it('it should fail when the email is a number', function(done) {
    chai.request(app)
    .post(signInURI)
    .send({ ...factory.validAdmin, email: 1234 })
    .end(function(err, res) {
        res.should.have.status(400)
        res.body.should.be.a('object')
        res.body.should.have.property('error').eql('email must be a string')
        done()
    })
})

it('it should fail when the email is an object', function(done) {
    chai.request(app)
    .post(signInURI)
    .send({ ...factory.validAdmin, email: { illegalProp: 1234 } })
    .end(function(err, res) {
        res.should.have.status(400)
        res.body.should.be.a('object')
        res.body.should.have.property('error').eql('email must be a string')
        done()
    })
})

it('it should fail when the password is a missing', function(done) {
    chai.request(app)
    .post(signInURI)
    .send({ ...factory.validAdmin, password: undefined })
    .end(function(err, res) {
        res.should.have.status(400)
        res.body.should.be.a('object')
        res.body.should.have.property('error').eql('password is required')
        done()
    })
})

it('it should fail when the password is an empty string', function(done) {
    chai.request(app)
    .post(signInURI)
    .send({ ...factory.validAdmin, password: '' })
    .end(function(err, res) {
        res.should.have.status(400)
        res.body.should.be.a('object')
        res.body.should.have.property('error').eql('password is not allowed to be empty')
        done()
    })
})

it('it should fail when the password is a number', function(done) {
    chai.request(app)
    .post(signInURI)
    .send({ ...factory.validAdmin, password: 1234 })
    .end(function(err, res) {
        res.should.have.status(400)
        res.body.should.be.a('object')
        res.body.should.have.property('error').eql('password must be a string')
        done()
    })
})

it('it should fail when the password is an object', function(done) {
    chai.request(app)
    .post(signInURI)
    .send({ ...factory.validAdmin, password: { illegalProp: 1234 } })
    .end(function(err, res) {
        res.should.have.status(400)
        res.body.should.be.a('object')
        res.body.should.have.property('error').eql('password must be a string')
        done()
    })
})

it('it should pass when a valid email and password is sent', function(done) {
    chai.request(app)
    .post(signInURI)
    .send(factory.validAdmin)
    .end(function(err, res) {
        res.should.have.status(200)
        res.body.should.be.an('object')
        res.header.should.have.property('x-auth-token')
        done()
    })
})

it('it should pass when a valid email and password plus an additional property is sent', function(done) {
    chai.request(app)
    .post(signInURI)
    .send({...factory.validAdmin, role: 'admin' })
    .end(function(err, res) {
        res.should.have.status(200)
        res.body.should.be.an('object').to.be.empty
        res.header.should.have.property('x-auth-token')
        done()
    })
})
