class ServiceError extends Error {
    constructor(reason = "something went wrong", statusCode = 522) {
        super(reason)
        Error.captureStackTrace(this, this.constructor)
        this.type = "ServiceError"
        this.name = this.constructor.name
        this.status = statusCode
    }
}

ServiceError.prototype.toSimpleJsonObject = function() {
    return  {
        error: {
            name: this.name,
            massage: this.message,
            status: this.status
        }
    }
}

module.exports = ServiceError
