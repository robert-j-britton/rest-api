exports.megabytes_to_bytes = (mb) => {
    return (1024 * 1024) * mb
}

exports.bytes_to_megabytes = (bytes) => {
    return (bytes / 1024) / 1024
}
