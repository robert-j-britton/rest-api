/**
 * Formats the *message* property of a Joi *Error* and prepends the path to the property that failed.
 *
 * @param {object} error Param `Error` instance produced by Joi
 * @returns {string} Returns `string` The Joi error message pulled from the `Error` object and formatted.
 */
function clean(error) {
    // Get the last error
    const _error = error.details.pop()
    // Removes the "property" from the start of the message
    let errorMsg = _error.message.split(' ').splice(1).join(' ')
    // Prepend the message with the path to the property that failed
    errorMsg = `[${_error.path.join('.')}]: ${errorMsg}`

    return errorMsg
}

module.exports.clean = clean
