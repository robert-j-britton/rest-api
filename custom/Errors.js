const ServiceError = require('./ServiceError');

class AccountServiceError extends ServiceError {}
class MailServiceError extends ServiceError {}
class MediaServiceError extends ServiceError {}
class TokenServiceError extends ServiceError {}

module.exports = {
    AccountServiceError,
    MailServiceError,
    MediaServiceError,
    TokenServiceError
}
