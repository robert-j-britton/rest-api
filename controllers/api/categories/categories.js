const _ = require('lodash')
const PostCategory = require('../../../models/PostCategory')
const slugify = require('slugify')


// Create a new category (allow only admin user to create a category)
exports.post_categories_create_one = (req, res, next) => {
    if (!req.auth.can) return res.status(403).send({ error: "access denied" })

    const { category } = req.body
    if (!category) return res.status(400).send({ error: 'category is required' })

    PostCategory.findOne({ title: category })
    .exec( async (err, doc) => {
        // Some internal db error occured. Return the error
        if (err) return res.status(500).send({ error: err.message })
        // Don't create duplicate category
        if (doc) return res.status(400).send({ error: `the category '${category}' already exists` });
        // Create the category
        const newCategory = new PostCategory({ title: category })
        // Return the category to the client
        await newCategory.save()
        res.send({ data: category });
    })
}

// Get all categories
exports.post_categories_get_all = (req, res, next) => {
    PostCategory.find({}).select('-__v -_id').exec((err, docs) => {
        // Some internal db error occured. Return the error
        if (err) return res.status(500).send({ error: err.message })
        res.send({ data: docs })
    })
}

// Update a category (allow only admin user to update a category)
exports.post_categories_update_one = async (req, res, next) => {
    if (!req.auth.can) return res.status(403).send({ error: "access denied" })

    const { category } = req.query

    // The undefined category should always remain the same, so don't allow this to be changed.
    if (category === "undefined" ) return res.status(400).send({ error: `the category '${category}' cannot be changed` })

    const updatedCategory  = req.body.category
    if (!updatedCategory) return res.status(400).send({ error: 'category is required' })
    const slugifiedUpdatedCategory = slugify(updatedCategory, { remove: /[`~!@#$%^&*()_|+=÷¿?;:'",\.<>\{\}\[\]\\\/]/gi, lower: true }).replace(/^\-+|\-+$/g, '')

    PostCategory.findOneAndUpdate({ title: category }, { title: slugifiedUpdatedCategory }, { new: true })
    .exec( async (err, doc) => {
        // Some internal db error occured. Return the error
        if (err) return res.status(500).send({ error: err.message })
        if (!doc) return res.status(400).send({ error: `the category '${category}' doesn't exist` });

        // check if the new category exists. We don't want duplicates
        const result = await PostCategory.findOne({ title: slugifiedUpdatedCategory })
        if (result) return res.status(400).send({ error: `the category '${slugifiedUpdatedCategory}' already exists`})
        res.send({ data: slugifiedUpdatedCategory });
    })
}

// Delete a category (allow only admin user to delete a category)
exports.post_categories_delete_one = async (req, res, next) => {
    if (!req.auth.can) return res.status(403).send({ error: "access denied" })
    // Don't allow the undefined category to be deleted by anyone, regardless of role. This is a kind of 'fallback' category.
    if (req.body.category === "undefined" ) return res.status(400).send({ error: "category 'undefined' cannot be deleted"})

    PostCategory.findOneAndDelete({ title: req.body.category })
    .exec((err, doc) => {
        if (err) return res.status(500).send({ error: err })
        if (!doc) return res.status(400).send({ error: "category doesn't exist" });
        res.send({ data: req.body.category });
    })
}
