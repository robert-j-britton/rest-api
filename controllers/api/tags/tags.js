const _ = require('lodash')
const PostTag = require('../../../models/PostTag')
const slugify = require('slugify')

// Create a new tag (allow only admin user to create a tag)
exports.post_tags_create_one = (req, res, next) => {
    if (!req.auth.can) return res.status(403).send({ error: "access denied" })

    const { tag } = req.body
    if (!tag) return res.status(400).send({ error: 'tag is required' })

    PostTag.findOne({ title: tag })
    .exec( async (err, doc) => {
        // Some internal db error occured. Return the error
        if (err) return res.status(500).send({ error: err.message })
        // Don't create duplicate tag
        if (doc) return res.status(400).send({ error: `the tag '${tag}' already exists` });
        // Create the tag
        const newTag = new PostTag({ title: tag })
        // Return the tag to the client
        await newTag.save()
        res.send({ data: tag });
    })
}

// Get all tags
exports.post_tags_get_all = (req, res, next) => {
    PostTag.find({}).select('-__v -_id').exec((err, docs) => {
        // Some internal db error occured. Return the error
        if (err) return res.status(500).send({ error: err.message })
        res.send({ data: docs })
    })
}

// Update a tag (allow only admin user to update a tag)
exports.post_tags_update_one = async (req, res, next) => {
    if (!req.auth.can) return res.status(403).send({ error: "access denied" })

    const { tag } = req.query

    const updatedTag  = req.body.tag
    if (!updatedTag) return res.status(400).send({ error: 'tag is required' })
    const slugifiedUpdatedTag = slugify(updatedTag, { remove: /[`~!@#$%^&*()_|+=÷¿?;:'",\.<>\{\}\[\]\\\/]/gi, lower: true }).replace(/^\-+|\-+$/g, '')

    PostTag.findOneAndUpdate({ title: tag }, { title: slugifiedUpdatedTag }, { new: true })
    .exec( async (err, doc) => {
        // Some internal db error occured. Return the error
        if (err) return res.status(500).send({ error: err.message })
        if (!doc) return res.status(400).send({ error: `the tag '${tag}' doesn't exist` });

        // check if the new tag exists. We don't want duplicates
        const result = await PostTag.findOne({ title: slugifiedUpdatedTag })
        if (result) return res.status(400).send({ error: `the tag '${slugifiedUpdatedTag}' already exists`})
        res.send({ data: slugifiedUpdatedTag });
    })
}

// Delete a tag (allow only admin user to delete a tag)
exports.post_tags_delete_one = async (req, res, next) => {
    if (!req.auth.can) return res.status(403).send({ error: "access denied" })

    PostTag.findOneAndDelete({ title: req.body.tag })
    .exec((err, doc) => {
        if (err) return res.status(500).send({ error: err })
        if (!doc) return res.status(400).send({ error: "tag doesn't exist" });
        res.send({ data: req.body.tag });
    })
}
