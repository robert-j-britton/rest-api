const _ = require('lodash')
const MediaService = require('../../../services/MediaService')


exports.uploads_create_one = async (req, res, next) => {
    try {
        const media = await MediaService.create(req.file, req.auth.uid)
        res.send({ data: media })
    }catch(error) {
        res.status(error.statusCode).send({ error: error.message })
    }
}

exports.uploads_delete_one = (req, res, next) => {

}
