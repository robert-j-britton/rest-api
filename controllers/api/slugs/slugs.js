const _ = require('lodash')
const Post = require('../../../models/Post')


exports.slug_get_preview = async (req, res, next) => {
    if (!req.query.slug) return res.status(400).send({ error: 'slug is required' })

    const { error, slug } = await Post().generateSlug(req.query.slug)
    if (error) return res.status(500).send({ error: err })
    res.send({ data: slug.url })
}
