const _ = require('lodash')
const Post = require('../../../models/Post')
const Global = require('../../../global')


exports.posts_create_one = async (req, res, next) => {
    const { auth } = req

    // Check if user is authorised to create a post
    if (!auth.can) return res.status(403).send({ error: Global.ACCESS_DENIED })

    // Obtain a valid slug for the given post title
    const { error, slug } = await Post().generateSlug(req.body.title)

    // Check if we get an error back from generate slug, return to client if we do
    if (error) return res.status(500).send({ error: err })

    // Assuming the req.body passed Joi validation checks define a post model
    // Add the author as the current user email. Add the generated slug details
    let post = new Post({
        ...req.body,
        author: auth.email,
        slug
    })

    // If the post is flagged as published, add the current date
    if (post.isPublished) post.publishedDate = Date.now();

    // Check that the post is valid, send error/s to client if invalid or post data if valid
    post.validate((err) => {
        if (err) {
            const keys = Object.keys(err.errors)

            let errors = {}
            keys.forEach(key => { errors[key] = err.errors[key].message })

            // Send list of errors to the client
            return res.status(400).send({ error: errors })
        }

        post.save()
        res.send( { data: _.pick(post, 'title', 'featureImageUrl', 'exerpt', 'body','category', 'tags', 'isPublished', 'slug.url', 'publishedDate') })
    })
}

exports.posts_get = (req, res, next) => {
    const page  = +req.query.page || 0
    const limit = +req.query.limit || 5
    const { count } = req.query

    // If count is 1 return document count otherwise return documents
    // TODO: Find a cleaner way of performing this action
    if (count && count == true) {

        Post.find({ ...req.filter })
        .countDocuments((err, count) => {
            if (err) return res.status(500).send({ error: err })
            res.send({ data: count })
        })

    } else {
        if (page === NaN || page < 0 ) return res.send([])
        if (limit === NaN || limit < 1 ) return res.send([])

        let populate = []
        if (req.query.comments == true) {
            populate.push('comments')
        }
        Post.find({ ...req.filter })
        .skip(page*limit)
        .limit(limit)
        .populate(populate.join(' '), '-__v')
        .select("-__v -_id -slug.version -slug.id")
        .exec((err, docs) => {
            if (err) return res.status(500).send({ error: err })
            res.send({ data: docs })
        })
    }


}

exports.posts_update_one = async (req, res, next) => {
    const { slug } = req.query
    const post  = req.body

    // Check if user has either authorisation to update posts or the post belongs to the user
    if (!req.auth.can && (post.author !== req.auth.email)) {
        return res.status(403).send({ error: Global.ACCESS_DENIED })
    }

    // Get the post to update
    const updatedPost = await Post.findOneAndUpdate({ 'slug.url': slug }, { ...post })

    // Check if a post exists
    if (!updatedPost) return res.status(400).send({ error: 'post does not exist'})

    res.send({ data: updatedPost })
}

exports.posts_delete_one = async (req, res, next) => {
    const { slug } = req.query

    // Get the post to delete
    const post = await Post.findOne({ 'slug.url': slug })

    // Check if a post exists
    if (!post) return res.status(400).send({ error: 'post does not exist'})

    // Check if user has either authorisation to delete posts or the post belongs to the user
    if (!req.auth.can && (post.author !== req.auth.email)) {
        return res.status(403).send({ error: Global.ACCESS_DENIED })
    }

    // Delete the post
    await Post.findOneAndDelete({ 'slug.url': slug })

    // Return the deleted post to the client
    res.send({ data: post })
}
