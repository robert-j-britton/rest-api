const _ = require('lodash')
const User = require('../../../models/Account')
const Global = require('../../../global')


// Update - Update a user if the client is the target or if client has authorisation to update users (admin user only for now, could let editors in future).
exports.users_user_update_one = (req, res, next) => {
    // User.findByIdAndUpdate(req.params.user_id, { ...req.body }, { new: true, useFindAndModify: false }, (err, user) => {
    //     if (err) return res.status(500).send({ error: err.message })
    //     if (!user) return res.status(404).send({ error: Global.USER_NOT_FOUND })
    //     const updatedUser = _.pick(user, [ '_id', 'username', 'email', 'name', 'role', 'joined', 'verified' ])
    //     res.send({ data: updatedUser })
    // })
    res.send({})
}

// Delete - Mark target user as inactive if the client is the target or if the client has authorisation to delete users (pretty much an admin thing.)
exports.users_user_delete_one = (req, res, next) => {
    // User.findByIdAndUpdate(req.params.user_id, { active: false }, { new: true, useFindAndModify: false }, (err, user) => {
    //     if (err) return res.status(500).send({ error: err.message })
    //     if (!user) return res.status(404).send({ error: Global.USER_NOT_FOUND})
    //     const deletedUser = _.pick(user, [ '_id', 'username', 'email', 'name', 'role', 'joined', 'verified', 'active' ])
    //     res.send({ data: deletedUser })
    // })
    res.send({})
}
