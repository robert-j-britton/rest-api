const _ = require('lodash')
const User = require('../../../models/User')
const Global = require('../../../global')


exports.users_me_get = (req, res, next) => {
    User.findById(req.auth.uid, (error, user) => {
        if (error) return res.status(500).send({ error: "something went wrong"})
        res.send({ data: user })
    })
}

// Update - Update a user if the client is the target or if client has authorisation to update users other than itself
exports.users_me_update = (req, res, next) => {
    User.findByIdAndUpdate(req.auth.uid, { ...req.body }, { new: true, useFindAndModify: false }, (err, user) => {
        if (err) return res.status(500).send({ error: err.message })
        if (!user) return res.status(404).send({ error: Global.USER_NOT_FOUND })
        const updatedUser = _.pick(user, [ '_id', 'username', 'email', 'name', 'role', 'joined', 'verified' ])
        res.send({ data: updatedUser })
    })
}

// Delete - Mark target user as inactive if the client is the target or if the client has authorisation to delete users other than itself
exports.users_me_delete = (req, res, next) => {
    User.findByIdAndUpdate(req.auth.uid, { active: false }, { new: true, useFindAndModify: false }, (err, user) => {
        if (err) return res.status(500).send({ error: err.message })
        if (!user) return res.status(404).send({ error: Global.USER_NOT_FOUND})
        const deletedUser = _.pick(user, [ '_id', 'username', 'email', 'name', 'role', 'joined', 'verified', 'active' ])
        res.send({ data: deletedUser })
    })
}
