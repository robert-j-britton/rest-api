const _ = require('lodash')
const Constants = require('../../../Constants')
const MailService = require('../../../services/MailService')
const TokenService = require('../../../services/TokenService')
const AccountService = require('../../../services/AccountService')
const Account = require('../../../models/Account')
const { JWT_HEADER_PAYLOAD_COOKIE, JWT_SIGNATURE_COOKIE } = Constants

// Create - Save a new user document to the collection
// todo: Add an AccountRegistrationReceivedEvent bus message.
exports.accounts_register = async (req, res, next) => {
    const { email, firstname, lastname, password } = req.body

    const name = `${firstname} ${lastname}`

    AccountService.register(email, name, password).then( async (account) => {
        const newAccount = _.omit(account.toJSON(), ['credentials.password', '__v'])
        res.send({ data: newAccount })
    })
    .catch(error => {
        console.log(error)
        if (error.name === "ValidationError") return res.status(400).send({ error: error.message })
        if (error.type === "ServiceError") return res.status(error.status).send(error.toSimpleJsonObject())
        res.status(500).send({ error: error.message })
    })
}

// Read - Get a valid token containing info about the user and flag user as active
exports.accounts_signin = (req, res, next) => {
    const { email, password } = req.body

    AccountService.signIn(email, password).then((data) => {
        let { account, token } = data

        // Generate a permanent cookie that contains the Jwt signature
        res.cookie(JWT_HEADER_PAYLOAD_COOKIE.name, token[0], JWT_HEADER_PAYLOAD_COOKIE.options)

        // Generate a HttpOnly permanent cookie that contains the Jwt signature
        res.cookie(JWT_SIGNATURE_COOKIE.name, token[1], JWT_SIGNATURE_COOKIE.options)

        // cleanup user data
        account = _.omit(account.toJSON(), ['credentials.password'])

        res.send({ data: account })
    })
    .catch(error => {
        // Expire Cookie
        res.cookie(JWT_HEADER_PAYLOAD_COOKIE.name, null, {
            ...JWT_HEADER_PAYLOAD_COOKIE.options,
            maxAge: -JWT_HEADER_PAYLOAD_COOKIE.options.maxAge
        })

        // Expire Permenant Cookie
        res.cookie(JWT_SIGNATURE_COOKIE.name, null, {
            ...JWT_SIGNATURE_COOKIE.options,
            maxAge: -JWT_HEADER_PAYLOAD_COOKIE.options.maxAge
        })

        res.status(500).send({ error: error.message })
    })
}

exports.accounts_reset_password = (req, res, next) => {
    // get email from client via form
    // send token to email address
    res.send({})
}

exports.accounts_verify = async (req, res, next) => {
    let token = req.headers.authorization
    if (!token) return res.status(400).send({ error: 'missing token' })
    token = token.split(' ')

    // Checks the header recieved is in 2 parts 'Bearer token'
    if (token.length !== 2) return res.status(400).send({ error: 'invalid token' })

    // Seperate the token from the Bearer prefix
    token = token.splice(1, 1).join('')

    // Validate token and get decoded token
    const decodedToken = TokenService.validateJwtToken(token)
    // If no token (invalid) return error
    if (!decodedToken) return res.status(400).send({ error: "token invalid or expired" })

    // Check the account exists and that it is not yet verified
    const account = await Account.findOne({ 'user._id': decodedToken.uid }, '-credentials.password')
    if (_.isEmpty(account)) return res.status(400).send({ error: 'account does not exist' })
    if(account.credentials.verified) return res.status(400).send({ error: 'account already verified'})

    // Change verified flag to true
    account.credentials.verified = true
    await account.save()

    // Create a public User record on db
    await AccountService.createUser(account.user._id, account.user.name)
    res.send({ data: account.credentials.email })
}

// Send a verification email to the address provided, only where it exists within an account.
exports.accounts_verify_resend = async (req, res, next) => {
    const { email } = req.body
    const account = await Account.findOne({ 'credentials.email': email }, '-credentials.password')
    // If there's no account with the given email, send back the email (this isn't an error)
    if (_.isEmpty(account)) return res.send({ data: email })
    if (account.credentials.verified) return res.status(400).send({ error: 'account already verified' })

    const token = TokenService.generateJwtToken({ uid: account.user._id }, "30m")

    await MailService.sendAccountVerificationEmail(email, { name: account.user.firstname, token  } )

    res.send({ data: email })
}
