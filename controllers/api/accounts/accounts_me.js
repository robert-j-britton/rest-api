const _ = require('lodash')
const Account = require('../../../models/Account')
const Constants = require('../../../Constants')


exports.accounts_me_get = (req, res, next) => {
    Account.findOne({ 'user._id': req.auth.uid }, '-credentials.password', (error, account) => {
        console.log('hello')
        if (error) return res.status(500).send({ error: "something went wrong"})
        res.send({ data: account })
    })
}

// Update - Update a user if the client is the target or if client has authorisation to update users other than itself
exports.accounts_me_update = async (req, res, next) => {
    const { user, credentials } = req.body
    // Get password from client and validate
    // Account.findByIdAndUpdate(req.auth.uid, { ...req.body }, { new: true, useFindAndModify: false }, (err, user) => {
    //     if (err) return res.status(500).send({ error: err.message })
    //     if (!user) return res.status(404).send({ error: Constants.USER_NOT_FOUND })
    //     const updatedUser = _.pick(user, [ '_id', 'username', 'email', 'name', 'role', 'joined', 'verified' ])
    //     res.send({ data: updatedUser })
    // })
    await req.account.save()

    // TODO: update User docs (and posts.author...) with user portion of accounts if changed to keep docs consistent


    const account = _.omit(req.account.toJSON(), ['credentials.password'])
    res.send({ data: account })
}

// Delete - Mark target user as inactive if the client is the target or if the client has authorisation to delete users other than itself
exports.accounts_me_delete = (req, res, next) => {
    // Get password from client and validate
    // Account.findByIdAndUpdate(req.auth.uid, { active: false }, { new: true, useFindAndModify: false }, (err, user) => {
    //     if (err) return res.status(500).send({ error: err.message })
    //     if (!user) return res.status(404).send({ error: Constants.USER_NOT_FOUND})
    //     const deletedUser = _.pick(user, [ '_id', 'username', 'email', 'name', 'role', 'joined', 'verified', 'active' ])
    //     res.send({ data: deletedUser })
    // })
    res.send({})
}
