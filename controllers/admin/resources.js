const _ = require('lodash')
const Resource = require('../../models/Resource')


exports.resources_create_one = async (req, res, next) => {
    const { type, roleAccess } = req.body

    // Check if the resource exists before creating
    const resource = await Resource.findOne({ type })
    if (!_.isEmpty(resource)) return res.status(400).send({ error: 'resource type already exists' })

    const newResource = new Resource({ type, roleAccess })

    newResource.save().then(resource => {
        res.send({ data: resource })
    })
    .catch(error => {
        res.send(400).send({ error: error.message })
    })
}

exports.resources_get_all = async (req, res, next) => {
    const resources =  await Resource.find({}, '-__v').lean()
    res.send({ data: resources })
}

exports.resources_resource_update_one = async (req, res, next) => {
    const { resource_id } = req.params
    const { type, roleAccess } = req.body

    // Get resource by id, return error if resource empty
    let oldResource = await Resource.findById(resource_id, '-__v')
    if (_.isEmpty(oldResource)) return res.status(400).send({ error: 'the resource specified doesn\'t exist' })

    // If type is set and the type differs from the old resource type, check the type doesn't exist to avoid duplicate resources.
    if(type && oldResource.type !== type) {
        // check resource update doesn't exist. res error if exists
        const newResource = await Resource.findOne({ type }, '_id')
        if (!_.isEmpty(newResource)) {
            return res.status(400).send({ error: 'the new resource type conflicts with an existing resource'})
        }
    }


    oldResource.type = type || oldResource.type

    // Update each roleAccess property recieved
    if (!_.isEmpty(roleAccess)) {
        Object.keys(roleAccess).forEach(key => {
            oldResource.roleAccess[key] = roleAccess[key]
        })
    }

    await oldResource.save()

    // return updated resource id to client
    res.send({ data: oldResource })
}

exports.resources_resource_delete_one = async (req, res, next) => {
    const {resource_id} = req.params

    const resource =  await Resource.findById(resource_id)
    if(_.isEmpty(resource)) return res.status(400).send({ error: 'the resource doesn\'t exist' })

    await resource.remove()

    res.send({ data: resource })
}
