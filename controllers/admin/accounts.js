const _ = require('lodash')
const Account = require('../../models/Account')


// Read - Get a list of users, filtered as specified by the client if the client has authorisation to read user data - this route is intended for admin users
exports.accounts_get_all = (req, res, next) => {
    const page  = +req.query.page || 0
    const limit = +req.query.limit || 10

    if (page === NaN || page < 0 ) return res.send([])
    if (limit === NaN || limit < 1 ) return res.send([])

    Account.find({}).select('-credentials.password -__v')
        .skip(page*limit)
        .limit(limit)
        .exec((err, accounts) => {
            if (err) return res.status(400).send({ error: err.message })
            res.send({ data: accounts })
        })
}

exports.accounts_account_update_one = (req, res, next) => {
    res.send({})
}

exports.accounts_account_delete_one = (req, res, next) => {
    res.send({})
}
