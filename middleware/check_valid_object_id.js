const { ObjectId } = require('mongoose').Types.ObjectId
const Constants = require('../Constants')


module.exports = (pathToObjectId, handleError = true) => {
    return async (req, res, next) => {
        const path = pathToObjectId.split('.')
        let id = req[path[0]][path[1]]

        req.validObjectId = ObjectId.isValid(id)

        // Continue to next middleware if the param is a valid ObjectId
        if (req.validObjectId) {
            next()
            return
        }

        if (!handleError && !req.validObjectId) {
            next()
            return
        }

        return res.status(404).send({ error: Constants.NOT_FOUND })
    }
}
