const multer = require('multer')

const fileFilter = (req, file, next) => {
    if ((/image\/(gif|jpg|jpeg|tiff|png)$/i).test(file.mimetype)) {
       next(null, true)
       return
    }
    next(null, false)
}

const upload = multer({
        limits: {
            fileSize: 1024 * 1024 * 5
        },
        fileFilter
})

module.exports = upload
