const Joi = require('@hapi/joi')
const _ = require('lodash')

module.exports = (req, res, next) => {
    // Keep only the properties we require by removing any additional properties the client may have added
    req.body = _.pick(req.body, ['email', 'password'])

    const { error } = validateUserSignIn(req.body);
    if (error) {
        let errMsg = error.details.pop().message.replace(/\"/g , '')
        return res.status(400).send({ error: errMsg })
    }
    next()
}

const validateUserSignIn = (user) => {
    const schema = {
        email: Joi.string().trim().email().required(),
        password: Joi.string().max(70).required()
    }
    return Joi.validate(user, schema)
}
