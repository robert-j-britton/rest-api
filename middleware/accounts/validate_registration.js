const Joi = require('@hapi/joi')
const JoiErrorCleaner = require('../../custom/JoiErrorCleaner')
const _ = require('lodash')

/**
 * Validation of the expected request body done prior to storage.
 */
module.exports = (req, res, next) => {
    // Keep only the properties we require by removing any additional properties the client may have added
    req.body = _.pick(req.body, ['email', 'password', 'firstname', 'lastname'])

    const { error, value } = validateUserSignUp(req.body);

    req.body = {
        ...value,
        name: `${value.firstname} ${value.lastname}`
    }

    if (error) {
        const message = JoiErrorCleaner.clean(error)
        return res.status(400).send({ error: message })
    }
    next()
}

const validateUserSignUp = (user) => {
    const schema = {
        email: Joi.string().email({ minDomainSegments: 2 }).required(), // Character limit for each part of the domain is 63 and the entire domain limit is 254
        firstname: Joi.string().trim().min(1).max(75).regex(/^[a-z]+$/).lowercase().required(),
        lastname: Joi.string().trim().min(1).max(75).regex(/^[a-z]+$/).lowercase().required(),
        password: Joi.string().trim().min(6).max(64).required(),
    }

    return Joi.validate(user, schema)
}
