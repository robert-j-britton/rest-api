module.exports = (resource_name) => {
    return (req, res, next) => {
        req.resource = resource_name
        next()
    }
}
