const Joi = require('joi')
const _ = require('lodash')
const JoiErrorCleaner = require('../../custom/JoiErrorCleaner')


module.exports = (req, res, next) => {
    req.body = _.pick(req.body, ['type', 'roleAccess'])


    const { error, value } = validate(req.body)
    if(error) {
        const errMsg = JoiErrorCleaner.clean(error)
        return res.status(400).send({ error: errMsg })
    }

    req.body = value

    next()
}

function validate(resource) {
    const schema = {
        type: Joi.string().trim().min(1).max(75).regex(/^[a-z]+$/).lowercase().required(),
        roleAccess: Joi.object({
            admin: Joi.number().integer().min(0).max(15),
            editor: Joi.number().integer().min(0).max(15),
            author: Joi.number().integer().min(0).max(15),
            reader: Joi.number().integer().min(0).max(15)
        }).required()

    }

    return Joi.validate(resource, schema)
}
