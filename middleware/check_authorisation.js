const _ = require('lodash')
const Resource = require('../models/Resource')
const checkAuthentication = require('./check_authentication')
const Constants = require('../Constants')


/**
 * Acquires the cookies from request, assembles token then validates.
 * Adds `req.auth` which contains the decoded token if valid.
 * Returns response with error status if token invalid, ending the middleware chain.
 *
 * Returns response with error status if no permission, ending the middleware chain..
 * Calls `next()` if client has permission.
 */
module.exports = (operation) => {
    return [ checkAuthentication, async (req, res, next) => {
        // Get the decoded token (provided by checkAuthorisation).
        const token = req.auth

        // Get the name of the resource
        const resource = req.resource

        checkPermission(token.role, resource, operation)
            .then(can => {
                if (!can) {
                    res.status(403).send({ error: Constants.ACCESS_DENIED })
                    return
                }
                next()
            })
            .catch(error => {
                // TODO: Return a 400 || 500 with generic message and log error
                res.status(403).send({ error: Constants.ACCESS_DENIED })
                return
            })
        }
    ]
}


function checkPermission(role_type, resource_type, operation) {
    return new Promise(async (resolve, reject) => {
        try {
            const resource = await Resource.findOne({ type: resource_type }, '-_id roleAccess')
            if (_.isEmpty(resource)) return resolve(false)

            // Obtain the access number assigned to role in role_access object
            const access = resource.roleAccess[role_type]
            if (!access) return resolve(false)

            // Check the access against the operation (crud)
            if(access & operation) return resolve(true)

            resolve(false)

        } catch(error) {
            reject(error)
        }
    })
}
