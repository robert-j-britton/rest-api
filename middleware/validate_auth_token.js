const { JWT_HEADER_PAYLOAD_COOKIE, JWT_SIGNATURE_COOKIE } = require('../Constants')

// Send response to client if there's an error in req.auth
module.exports = (req, res, next) => {
    const { status, error } = req.auth
    if (error) {
        // Expire the cookie
        res.cookie(JWT_HEADER_PAYLOAD_COOKIE.name, '',
            {
                ...JWT_HEADER_PAYLOAD_COOKIE.options,
                maxAge: -JWT_HEADER_PAYLOAD_COOKIE.options.maxAge
            })
            .status(status)
            .send({ error: error })

        return
    }

    // Refresh cookie
    res.cookie(JWT_HEADER_PAYLOAD_COOKIE.name, req.cookies.auth_token, JWT_HEADER_PAYLOAD_COOKIE.options)

    // Refresh cookie
    res.cookie(JWT_SIGNATURE_COOKIE.name, req.cookies.auth_token_sig, JWT_SIGNATURE_COOKIE.options)

    next()
}
