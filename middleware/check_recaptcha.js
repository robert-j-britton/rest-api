const request = require('request')
const _ = require('lodash')


module.exports = (req, res, next) => {
    if (!req.body.recaptcha) res.status(400).send({error: 'recaptcha verification failed'})
    const verifyURL = `https://google.com/recaptcha/api/siteverify?secret=${process.env['recaptcha-secret-key']}&response=${req.body.recaptcha}`
    request(verifyURL, (err, response, body) => {
        body = JSON.parse(body)
        if (!body.success) {
            return res.status(400).send({error: 'recaptcha verification failed'})
        }
        delete req.body.recaptcha
        next()
    })
}
