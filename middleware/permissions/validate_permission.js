const Joi = require('joi')
Joi.objectId = require('joi-objectid')(Joi);

const _ = require('lodash')
const JoiErrorCleaner = require('../../custom/JoiErrorCleaner')


module.exports = (req, res, next) => {
    req.body = _.pick(req.body, ['role', 'resource', 'access'])

    const { error, value } = validate(req.body)
    if(error) {
        const errMsg = JoiErrorCleaner.clean(error)
        return res.status(400).send({ error: errMsg })
    }

    req.body = value

    next()
}

function validate(resource) {
    const schema = {
        role: Joi.string().trim().required(),
        permissions: Joi.array().items(
            Joi.object().keys({
                resource: Joi.string().trim().required(),
                access: Joi.number().integer().min(0).max(15).required()
            }).required()
        ).required()
    }

    return Joi.validate(resource, schema)
}
