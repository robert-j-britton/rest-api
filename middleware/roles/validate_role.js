const Joi = require('@hapi/joi')
const _ = require('lodash')
const JoiErrorCleaner = require('../../custom/JoiErrorCleaner')


module.exports = (req, res, next) => {
    req.body = _.pick(req.body, ['type'])

    const { error, value } = validate(req.body)
    if(error) {
        const errMsg = JoiErrorCleaner.clean(error)
        return res.status(400).send({ error: errMsg })
    }

    req.body = value

    next()
}

function validate(role) {
    const schema = {
        type: Joi.string().trim().min(1).max(75).alphanum().lowercase().required()
    }

    return Joi.validate(role, schema)
}
