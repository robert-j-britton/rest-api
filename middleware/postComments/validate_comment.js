const Joi = require('@hapi/joi')


module.exports = (req, res, next) => {
    const { error } = validatePost(req.body);
    if (error) {
        let errMsg = error.details.pop().message.replace(/\"/g , '')
        return res.status(400).send({ error: errMsg })
    }
    next()
}

const validatePost = (post) => {
    const schema = {
        body: Joi.string().trim().max(1000).required()
    }
    return Joi.validate(post, schema)
}
