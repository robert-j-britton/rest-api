const Joi = require('@hapi/joi')
const _ = require('lodash')

module.exports = (req, res, next) => {

    req.body = _.pick(req.body, ['email'])

    const { error } = validateUserSignIn(req.body);
    if (error) {
        let errMsg = error.details.pop().message.replace(/\"/g , '')
        return res.status(400).send({ error: errMsg })
    }
    next()
}

const validateUserSignIn = (email) => {
    const schema = {
        email: Joi.string().trim().email().required(),
    }
    return Joi.validate(email, schema)
}
