const Joi = require('joi')
const _ = require('lodash')
const JoiErrorCleaner = require('../custom/JoiErrorCleaner')


module.exports = (req, res, next) => {
    // Check the file was recieved
    if(_.isEmpty(req.file)) return res.status(400).send({ error: 'invalid or no file recieved'})

    // Get the alt text (if recieved)
    let meta = _.pick(req.body, ['alt'])

    // If alt is provided, validate it and place in the gievn file.
    if (meta.alt) {
        const { error, value } = validate(meta)
        if(error) {
            const errMsg = JoiErrorCleaner.clean(error)
            return res.status(400).send({ error: errMsg })
        }

        req.file.alt = value.alt
    }

    next()
}

function validate(resource) {
    const schema = {
        alt: Joi.string().trim().min(1).max(75).lowercase()
    }

    return Joi.validate(resource, schema)
}
