const isEmpty = require('lodash').isEmpty
const isValid = require('mongoose').Types.ObjectId.isValid

module.exports = (req, res, next) => {
    let invalidKeys = []
    Object.keys(req.params).forEach(key => { if (!isValid(req.params[key])) invalidKeys.push(key) })

    if (!isEmpty(invalidKeys)) return res.status(400).send({ error: 'invalid objectid' })

    next()
}
