const Role = require('../models/Role')


module.exports = async (req, res, next) => {
    const role = await Role.findOne({ role: req.auth.role })
    req.body.role = role._id
    next()
}
