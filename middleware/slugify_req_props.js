const slugify = require('slugify')


/**
 * param (props) type: string or [string] \
 * example call 1: slugifyReqPorps('body.category') single property \
 * example call 2: slugifyReqPorps(['body.category', 'query.tag', 'body.title' ]) multiple properties
 */
module.exports = (props) =>  {
    return (req, res, next) => {
        if (typeof props === "object") {
            props.forEach(prop => {
                const p = prop.split('.', 2)
                if (req[p[0]][p[1]]) {
                    req[p[0]][p[1]] = slugify(req[p[0]][p[1]], { remove: /[`~!@#$%^&*()_|+=÷¿?;:'",\.<>\{\}\[\]\\\/]/gi, lower: true }).replace(/^\-+|\-+$/g, '')
                }
            });
        } else if (typeof props === "string") {
            const p = props.split('.', 2)
            if (req[p[0]][p[1]]) {
                req[p[0]][p[1]] = slugify(req[p[0]][p[1]], { remove: /[`~!@#$%^&*()_|+=÷¿?;:'",\.<>\{\}\[\]\\\/]/gi, lower: true }).replace(/^\-+|\-+$/g, '')
            }
        }

        next()
}}
