const Constants = require('../Constants')
const TokenService = require('../services/TokenService')


// Gets the token split over 2 cookies, reconstructs then returns decoded token or error in auth property
module.exports = (req, res, next) => {
    const { auth_token, auth_token_sig } = req.cookies

    // If either cookie is missing, set auth error
    if (!auth_token || !auth_token_sig) {
        req.auth = { status: 401, error: Constants.AUTH_FAIL_MISSING_TOKEN }
        return next()
    }

    let token = TokenService.combineJwtToken(auth_token, auth_token_sig)
    token = TokenService.validateJwtToken(token)

    // if the token is undefined, set auth error
    if (!token) {
        req.auth = { status: 400, error: Constants.AUTH_FAIL_INVALID_TOKEN }
        return next()
    }

    // return auth decoded token if all checks pass
    req.auth = token
    next()
}
