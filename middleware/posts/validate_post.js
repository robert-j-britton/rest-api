const Joi = require('@hapi/joi')
const _ = require('lodash')


module.exports = (req, res, next) => {
    req.body = _.pick(req.body, ['title', 'exerpt', 'body', 'category', 'featureImageUrl', 'tags', 'isPublished'])

    const { error } = validatePost(req.body);
    if (error) {
        let errMsg = error.details.pop().message.replace(/\"/g , '')
        return res.status(400).send({ error: errMsg })
    }
    next()
}

const validatePost = (post) => {
    const schema = {
        title: Joi.string().trim().required().max(254),
        exerpt: Joi.string().trim(),
        body: Joi.object().required(),
        category: Joi.string().trim().required(),
        featureImageUrl: Joi.string(),
        tags: Joi.array(),
        isPublished: Joi.boolean().required()
    }
    return Joi.validate(post, schema)
}
