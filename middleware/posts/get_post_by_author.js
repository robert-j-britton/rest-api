module.exports = (req, res, next) => {
    const { author } = req.query

    const authorFilter = author ? { author: author } : undefined

    req.filter = {
        ...req.filter,
        ...authorFilter
    }

    next()
}
