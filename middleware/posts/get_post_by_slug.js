const Post = require('../../models/Post')


module.exports = (req, res, next) => {
    const { slug } = req.query

    // if slug is given, return the post (only if it is published)
    if (slug) {
        // We should perform a check if user has authorisation to read draft posts
        // or if draft post belongs to the user, if we intend to send a non-published post

        Post.findOne({ 'slug.url': slug })
        .select('-__v -_id -slug.version -slug.id')
        .exec((err, doc) => {
            // Internal mongodb error
            if (err) return res.status(500).send({ error: err })

            // Send 404 to client if no post exists
            if (!doc) return res.status(404).send({ error: 'could not find post/s with the paramaters provided' })

            // Send post to client if it's published. No auth required
            if (doc.isPublished) return res.send({ data: doc })

            // Check user authentication and authorisation
            if (req.auth.error || (req.auth.email !== doc.author)) {
                return res.status(404).send({ error: 'could not find post/s with the paramaters provided' })
            }

            // Return the un-published post to client
            return res.send({ data: doc })
        })
    } else {
        next()
    }
}
