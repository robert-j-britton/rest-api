module.exports = (req, res, next) => {
    const { tag } = req.query

    const tagFilter = tag ? { tags: { $all: tag } } : undefined

    req.filter = {
        ...req.filter,
        ...tagFilter
    }

    next()
}
