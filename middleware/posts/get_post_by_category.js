module.exports = (req, res, next) => {
    const { category } = req.query

    const categoryFilter = category ? { category: category } : undefined

    req.filter = {
        ...req.filter,
        ...categoryFilter
    }

    next()
}
