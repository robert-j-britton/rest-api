module.exports = (req, res, next) => {
    const { published } = req.query

    let publishedFilter = { isPublished: true }

    if (published && published === 'false') {
        // Check user is authenticated (signed in)
        const { auth } = req
        if (auth.error) return res.status(auth.status).send({ error: auth.error })
        req.query.author = auth.email
        publishedFilter = { isPublished: false }
    }

    req.filter = {
        ...req.filter,
        ...publishedFilter
    }

    next()
}
