const _ = require('lodash')
const Account = require('../models/Account')
const Constants = require('../Constants')


/**
 * Call after authentication, used where a password is required when user wants to update their account information.
 * Where the user is authenticated and password is valid, the account is copied to the `req.account` object.
 *
 */
module.exports = async (req, res, next) => {
    // checks authenticated. Server error if no auth.
    if (_.isEmpty(req.auth)) return res.status(500).send({ error: 'missing authentication' })

    // get password from request body. Return error if missing.
    const { password } = _.pick(req.body, ['password'])
    if (!password) return res.status(400).send({ error: 'password is required' })

    // get the client account by user id (from auth object).
    const account = await Account.findOne({ 'user._id': req.auth.uid })

    // check password match. return error is no match, place account ref in req account object if match.
    const passwordValid = account.credentials.validatePassword(password)
    if (!passwordValid) return res.status(400).send({ error: 'incorrect password' })

    // done with password checking so remove the password from request.
    delete req.body.password

    req.account = account

    next()
}


