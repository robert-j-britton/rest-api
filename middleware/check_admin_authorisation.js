const _ = require('lodash')
const checkAuthentication = require('./check_authentication')
const Constants = require('../Constants')


/**
 * Acquires the cookies from request, assembles token then validates.
 * Adds `req.auth` which contains the decoded token if valid.
 * Returns response with error status if token invalid, ending the middleware chain.
 *
 * Returns response with error status if token.role not admin, ending the middleware chain..
 * Calls `next()` if token.role is admin.
 */
module.exports = [ checkAuthentication, (req, res, next) => {
        // Get the decoded token (provided by checkAuthorisation).
        const token = req.auth
        if (token.role !== "admin")  {
            res.status(403).send({ error: Constants.ACCESS_DENIED })
            return
        }

        next()
    }]

