const Constants = require('../Constants')
const TokenService = require('../services/TokenService')
const { JWT_HEADER_PAYLOAD_COOKIE, JWT_SIGNATURE_COOKIE } = require('../Constants')

/**
 * Acquires the cookies from request, assembles token then validates.
 * Add decoded token to `req.auth` if valid, then calls next().
 * Responds to client if token invalid ending the middleware chain.
 */
module.exports = (req, res, next) => {
    const { auth_token, auth_token_sig } = req.cookies
    // If either cookie is missing, return out with a 401
    if (!auth_token || !auth_token_sig) {
        return res.status(401).send({ error: Constants.AUTH_FAIL_MISSING_TOKEN })
    }

    // Combine the token from cookies then validate
    let token = TokenService.combineJwtToken(auth_token, auth_token_sig)
    token = TokenService.validateJwtToken(token)

    // If the token is not valid, return out with a 400
    if (!token) {
        res.cookie(JWT_HEADER_PAYLOAD_COOKIE.name, '',
        {
            ...JWT_HEADER_PAYLOAD_COOKIE.options,
            maxAge: -JWT_HEADER_PAYLOAD_COOKIE.options.maxAge
        })
        res.status(400).send({ error: Constants.AUTH_FAIL_INVALID_TOKEN })
        return
    }

    // If all is valid add the token to req.auth refresh the cookies.

    req.auth = token

    // Refresh response cookies
    res.cookie(JWT_HEADER_PAYLOAD_COOKIE.name, req.cookies.auth_token, JWT_HEADER_PAYLOAD_COOKIE.options)
    res.cookie(JWT_SIGNATURE_COOKIE.name, req.cookies.auth_token_sig, JWT_SIGNATURE_COOKIE.options)

    next()
}
