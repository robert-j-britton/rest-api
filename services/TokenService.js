'use strict'
const appRoot = require('app-root-path')
const jwt = require('jsonwebtoken')
const fs = require('fs')

module.exports.generateJwtToken = generateJwtToken
module.exports.splitJwtToken = splitJwtToken
module.exports.combineJwtToken = combineJwtToken
module.exports.validateJwtToken = validateJwtToken

function generateJwtToken(payload, expiresIn = "12h") {
    const privateKey = fs.readFileSync(`${appRoot}/private.key`, 'utf8')
    const token = jwt.sign(payload, privateKey, {
        expiresIn,
        algorithm: "RS256"})
    return token
}

function splitJwtToken (token) {
    let token_split = token.split('.')
    const signature = token_split.pop()
    token_split = token_split.join('.').split(' ')
    token_split.push(signature)
    return token_split
}

function combineJwtToken (token_header_payload, token_signature) {
    return `${token_header_payload}.${token_signature}`
}

function validateJwtToken(token) {
    const publicKey = fs.readFileSync(`${appRoot}/public.key`, 'utf8')
    try {
        const _token = jwt.verify(token, publicKey, {  algorithms: ["RS256"] })
        return _token
    } catch (err) {
        return undefined
    }
}
