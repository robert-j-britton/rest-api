const _ = require('lodash')
const nodemailer = require('nodemailer')
const hbs = require('nodemailer-express-handlebars')
const appRoot = require('app-root-path')

module.exports.sendAccountVerificationEmail = sendAccountVerificationEmail


// create reusable transporter object using the default SMTP transport
const transporter = nodemailer.createTransport({
    service: "Outlook365",
    port: 465,
    secure: true,
    auth: {
        user: process.env.smtp_user,
        pass: process.env.smtp_password
    }
})

transporter.use('compile', hbs({
    viewEngine: {
        partialsDir: 'some/view',
        layoutsDir: appRoot + '/views/layouts',
        defaultLayout: 'main.hbs',
        extName: '.hbs'
    },
    viewPath: appRoot + '/views',
    extName: '.hbs'
}))

function sendAccountVerificationEmail(to, payload = { name: "", token: "" }) {
    const mailOptions = {
        from: '"donotreply" <donotreply@apexsolution.co.uk>',
        to,
        subject: "Apex Solution - verify account",
        template: "account_verify",
        context: {
            name: payload.name,
            token: payload.token
        }
    }

    return new Promise((resolve, reject) => {
        transporter.sendMail(mailOptions, (error, info) => {
            if (error) return reject(error)
            resolve(info)
        })
    })
}
