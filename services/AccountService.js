const _ = require('lodash')
const { AccountServiceError } = require('../custom/Errors')
const Constants = require('../Constants')
const Account = require('../models/Account')
const User = require('../models/User')
const Role = require('../utility/Roles')
const TokenService = require('./TokenService')

module.exports.register = register
module.exports.signIn = signIn
module.exports.createUser = createUser

function register(email, name, password) {
    return new Promise(async (resolve, reject) => {
        try {
            const emailTaken = await isEmailTaken(email)
            if (emailTaken) return reject(new AccountServiceError(Constants.EMAIL_ALREADY_REGISTERED, 400))

            // Create the account doc for private use. (and possibly admin)
            let account = new Account({
                user: { name },
                credentials: { email, password },
                role: Role.default
            })

            // Save Account document to db.
            await account.save()

            // Return the account doc.
            resolve(account)

        } catch(error) {
            reject(error)
        }
    })
}

function createUser(user_id, name) {
    return new Promise(async (resolve, reject) => {
        try {
            // Create a User doc for saving the user record for public consumption.
            await new User({ _id: user_id, name }).save()

            resolve()
        } catch(error) {
            reject(error)
        }
    })
}

function signIn(email, password) {
    return new Promise(async (resolve, reject) => {
        try {
            // get the account associated with the given email and populate the user.
            let account = await findAccountByEmail(email)
            if (_.isEmpty(account)) return reject(new AccountServiceError(Constants.INVALID_EMAIL_PASSWORD, 400))

            const isValidPassword = account.credentials.validatePassword(password)
            if(!isValidPassword) return reject(new AccountServiceError(Constants.INVALID_EMAIL_PASSWORD, 400))

            // Reject with error if account not verified
            if (!account.credentials.verified) return reject(new AccountServiceError("account not verified", 403))

            // Update the public User document
            await User.findByIdAndUpdate(account.user._id, { active: true })

            // Update the private Account user active flag
            account.user.active = true
            await account.save()

            let token = TokenService.generateJwtToken({ uid: account.user._id, role: account.role })
            token = TokenService.splitJwtToken(token)

            resolve({ account: account, token })
        } catch(error) {
            reject(error)
        }
    })
}

function findAccountByEmail(email) {
    return Account.findOne({ 'credentials.email': email })
}

function isEmailTaken(email) {
    return new Promise(async (resolve, reject) => {
        try{
            // Find an account by email but select only the _id to save bandwidth
            const account = await findAccountByEmail(email).select('_id')
            if (_.isEmpty(account)) return resolve(false)
            resolve(true)
        }catch(error) {
            reject(error)
        }
    })
}
