const aws = require('aws-sdk')
const _ = require('lodash')
const ObjectId = require('mongoose').Types.ObjectId
const ServiceError = require('../custom/ServiceError')
const Media = require('../models/Media')


module.exports.create = create
module.exports.getStorageUsed = getStorageUsed

function getStorageUsed(user_id) {
    return new Promise(async (resolve, reject) => {
        try {
            const media = await Media.find({ user: user_id }, 'size')
            // if the user has no media return 0
            if (media.length === 0) return resolve(0)
            // If media is found, combine media sizes and return result
            const usage = media.reduce((prev, current) => ({ size: prev.size + current.size }))
            resolve(usage.size)
        } catch (error) {
            // return an error that happened between the request to mongodb service
            reject(error)
        }
    })
}

function create(file, user_id) {
    return new Promise( async (resolve, reject) => {
        try {
            // Obtain a unique id to set as upload destination name
            const media_id = new ObjectId()
            const fileDestName = `${media_id}`

            // Upload the file to s3 and update the files location
            const uploadLocation = await uploadToS3(file, fileDestName)
            file.location = uploadLocation

            // Create a media model using the media id, pass the file with the updated location and the user's id
            let media = new Media({
                user: user_id,
                file: {
                    _id: media_id, // _id also represents the S3 'Key' property, required to identify the object within the bucket
                    filename: file.originalname,
                    mimetype: file.mimetype,
                    alt: file.alt,
                    location: file.location,
                    size: file.size
                }
            })

            // Save the model, creating a document on the db
            await media.save()

            media = _.omit(media.toJSON(), ['__v', 'user'])
            resolve(media)
        } catch (error) {
            console.log(error)
            // ServiceErrors are passed back to caller
            if (error.name === "ServiceError" || error.name === "ValidationError") return reject(error)
            // All other errors are passed back with genric message
            // e.g s3 errors, mongoose validation errors, connection errors etc.
            reject(new ServiceError())
        }
    })
}

function s3Bucket() {
    aws.config.update({
        accessKeyId: process.env.aws_access_key_id,
        secretAccessKey: process.env.aws_secret_access_key,
        region: process.env.aws_region
    })
    return new aws.S3({ params: { Bucket: process.env.aws_bucket } })
}

function uploadToS3(file, destFileName) {
    const bucket = s3Bucket()

    // Promisify callback
    return new Promise((resolve, reject) => {
        bucket.upload({
            ACL: 'public-read',
            Body: file.buffer,
            Key: destFileName,
            ContentType: file.mimetype
        })
        .send((error, data) => {
            if (error) return reject(error)
            resolve(data.Location)
        })
    })
}
