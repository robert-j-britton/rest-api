const mongoose = require('mongoose')
const bcrypt = require('bcrypt')
const uniqueValidator = require('mongoose-unique-validator')


const AccountSchema = new mongoose.Schema({
    email: {
        type: String,
        index: true,
        unique: true,
        uniqueCaseInsensitive: true,
        required: [true, 'email is required'],
    },
    password: {
        type: String,
        required: [true, 'password is required'],
        set: function(pwd) {
            return this.generatePasswordHash(pwd)
        }
    },
    verified: {
        type: Boolean,
        default: false
    }
}, { _id: false })

AccountSchema.methods.generatePasswordHash = function(password) {
    const saltRounds = 10;
    const hash = bcrypt.hashSync(password, saltRounds, null)
    return hash
}

AccountSchema.methods.validatePassword = function(password) {
    const validPassword = bcrypt.compareSync(password, this.password);
    return validPassword;
}

AccountSchema.plugin(uniqueValidator, { message: '{PATH} is already in use' })

module.exports = AccountSchema
