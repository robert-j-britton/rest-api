const _ = require('lodash')
const mongoose = require('mongoose')


const PostMetaSchema = mongoose.Schema({
    title: {
        type: String,
        required: [true, "title is required"]
    },
    exerpt: {
        type: String,
        required: [true, 'exerpt is required']
    },
    category: {
        type: String,
        required: [true, 'category is required'],
        default: "undefined"
    },
    tags: {
        type: Array
    },
    slug: {
        type: String,
        required: [true, 'slug is required']
    },
})


module.exports = PostMetaSchema
