const mongoose = require('mongoose')


const SlugSchema = new mongoose.Schema({
    originalUrl: {
        type: String,
        required: [true, 'url is required']
    },
    version: {
        type: Number,
        required: [true, 'version is required'],
        default: 0
    }
})

SlugSchema.virtual('url').get(function() {
    return this.version === 0 ? this.originalurl : `${this.originalUrl}-${this.version}`
})

module.exports = SlugSchema
