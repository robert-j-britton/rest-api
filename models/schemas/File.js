const mongoose = require('mongoose')


const FileMetaSchema = new mongoose.Schema({
    filename: {
        type: String,
        required: [true, 'filename is required']
    },
    mimetype: {
        type: String,
        required: [true, 'mimetype is required']
    },
    alt: {
        type: String
    },
    location: {
        type: String,
        required: [true, 'location is required']
    },
    size: {
        type: Number,
        required: [true, 'size is required']
    }
})

module.exports = FileMetaSchema
