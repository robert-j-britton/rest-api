const mongoose = require('mongoose')


const RoleSchema = new mongoose.Schema({
    type: {
        type: String,
        required: [true, 'type is required']
    }
})


module.exports = RoleSchema
