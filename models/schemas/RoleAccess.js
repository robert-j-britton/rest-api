const mongoose = require('mongoose')


const RoleAccessSchema = new mongoose.Schema({
    admin: { type: Number, default: 0 },
    author: { type: Number, default: 0 },
    editor: { type: Number, default: 0 },
    reader: { type: Number, default: 0 }
}, { _id: false })


module.exports = RoleAccessSchema
