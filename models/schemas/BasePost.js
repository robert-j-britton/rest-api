const mongoose = require('mongoose')
const slugify = require('slugify')
const uniqueValidator = require('mongoose-unique-validator')
const FileSchema = require('./schemas/File')
const UserSchema = require('./schemas/User')


const BasePostSchema = new mongoose.Schema({
    author: {
        type: UserSchema,
        required: [true, 'author is required']
    },
    title: {
        type: String,
        required: [true, "title is required"]
    },
    exerpt: {
        type: String
    },
    body: {
        type: Object,
        required: [true, 'body is required'],
        default: {}
    },
    category: {
        type: String,
        required: [true, 'category is required'],
        default: "undefined"
    },
    featureImage: {
        type: FileSchema
    },
    tags: {
        type: Array,
        default: []
    },
    slug: {
        type: String,
        unique: true,
        index: true,
        required: [true, 'slug is required']
    }
})


module.exports = BasePostSchema
