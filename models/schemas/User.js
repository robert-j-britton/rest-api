const mongoose = require('mongoose')
const FileSchema = require('./File')


const UserSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, 'name is required'],
        // Capitalise each part of the fullname
        set: function(name) {
            const _name = name.toLowerCase()
            let capitalised = []
            _name.split(' ').forEach((value) => { capitalised.push(value.charAt(0).toUpperCase() + value.slice(1)) })
            return capitalised.join(' ')
        }
    },
    profileImage: {
        type: FileSchema,
    },
    active: {
        type: Boolean,
        required: [true, 'active is required'],
        default: true
    },
    biography: {
        type: String
    },
    joined: {
        type: Date,
        get: function() {
            return mongoose.Types.ObjectId(this._id).getTimestamp()
        }
    }
})

UserSchema.virtual('firstname').get(function() {
    console.log(this.name)
    return this.name.split(' ').splice(0, 1).join('')
})

UserSchema.virtual('lastname').get(function() {
    return this.name.split(' ').splice(1, 1).join('')
})

module.exports = UserSchema
