const mongoose = require('mongoose')

module.exports = mongoose.model('PostCategory', new mongoose.Schema({
    title: {
        type: String,
        required: true
    }
}))
