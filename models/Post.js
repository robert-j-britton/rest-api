const mongoose = require('mongoose')
const slugify = require('slugify')
const uniqueValidator = require('mongoose-unique-validator')
const FileSchema = require('./schemas/File')
const ProfileSchema = require('./schemas/Profile')


const PostSchema = new mongoose.Schema({
    author: {
        type: ProfileSchema,
        required: [true, 'author is required']
    },
    title: {
        type: String,
        required: [true, "title is required"]
    },
    exerpt: {
        type: String
    },
    body: {
        type: Object,
        required: [true, 'body is required'],
        default: {}
    },
    category: {
        type: String,
        required: [true, 'category is required'],
        default: "undefined"
    },
    featureImage: {
        type: FileSchema
    },
    tags: {
        type: Array,
        default: []
    },
    slug: {
        type: String,
        unique: true,
        index: true,
        required: [true, 'slug is required']
    }
})

PostSchema.methods.generateSlug = function(title) {
    const slugifiedTitle = slugify(title , {
        remove: /[`~!@#$%^&*()_|+=÷¿?;:'",\.<>\{\}\[\]\\\/]/gi, lower: true
    }).replace(/^\-+|\-+$/g, '') // Strips string, replace space with dash and allow only single dashes


    return new Promise((resolve, reject) => {
        // Find posts where slugified title matches the slug id. Get sorted list in ascending version order
        Post.find({ 'slug.id': slugifiedTitle }).select('slug -_id').sort({ 'slug.version': 1 }).exec((err, res) => {
            if (err) {
                return resolve({ error: err })
            }

            for (let index = 0; index < res.length+1; ++index) {
                // Check if either a slug doesn't exist at the given index or if (if a slug exists) the slug version matches index
                if (!res[index] || res[index].slug.version !== index) {
                    resolve({ slug: { id: slugifiedTitle, version: index, url: `/${index}/${slugifiedTitle}` }})
                    break
                }
            }
        })
    })
}

PostSchema.plugin(uniqueValidator, { message: '{PATH} is already in use' })

const Post = mongoose.model('Post', PostSchema)


const PublishedPost = Post.discriminator('Published',
    new mongoose.Schema({
        publishedOn: {
            type: Date,
            required: [true, 'publishedOn is required'],
            default: function() {
                return mongoose.Types.ObjectId(this._id).getTimestamp()
            }
        }
    }))

const BinnedPost = Post.discriminator('Binned',
    new mongoose.Schema({}))

module.exports = Post
