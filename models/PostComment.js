const mongoose = require('mongoose')

const PostCommentSchema = new mongoose.Schema({
    _id: {
        type: mongoose.SchemaTypes.ObjectId,
        auto: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: [true, 'comment user ref is required']
    },
    post: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Post',
        required: [true, 'comment post ref is required']
    },
    posted: {
        type: Date,
        get: function() {
            return mongoose.Types.ObjectId(this._id).getTimestamp()
        }
    },
    body: {
        type: String,
        required: [true, 'comment body is required']
    },
    responses: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Comment-Response'
    }],

})

PostCommentSchema.methods.addResponseRef = function(responseId) {
    const self = this

    return new Promise(resolve => {
        // Create a copy of comment array
        let updatedResponses = [...self.responses]
        // Check if the new responseId exists in the post.responses array
        const responseExists = updatedResponses.findIndex(r => r.equals(responseId))

        // Return the responseId if exists (this is not an error)
        if (responseExists !== -1) { return resolve(true) }

        // Push the new comment to the copied array and overwrite this commnts
        updatedResponses.push(responseId)
        self.responses = updatedResponses

        // Save this post
        self.save((err) => {
            if (err) return resolve(false)
            resolve(true)
        })
    })
}

PostCommentSchema.methods.removeResponseRef = function(responseId) {
    const self = this

    return new Promise(resolve => {
        let updatedResponses = [...self.responses]
        const responseIndex = updatedResponses.findIndex(response => response.equals(responseId))
        if (responseIndex === -1) return resolve(true)

        updatedResponses.splice(responseIndex, 1)
        // Overwrite responses with new array and save changes
        self.responses = updatedResponses
        self.save(err => {
            if (err) return resolve(false)
            resolve(true)
        })
    })
}

module.exports = mongoose.model('Post-Comment', PostCommentSchema)
