const mongoose = require('mongoose')
const SlugSchema = require('./schemas/Slug')

module.exports = mongoose.model('Slug', SlugSchema)
