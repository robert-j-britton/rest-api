const mongoose = require('mongoose')

module.exports = mongoose.model('Post-Tag', new mongoose.Schema({
    title: {
        type: String,
        required: true
    }
}))
