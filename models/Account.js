const mongoose = require('mongoose')
const UserSchema = require('./schemas/User')
const CredentialsSchema = require('./schemas/Credentials')


const AccountSchema = new mongoose.Schema({
    user: {
        type: UserSchema,
        required: [true, 'user is required']
    },
    credentials: {
        type: CredentialsSchema,
        required: [true, 'credentials is required']
    },
    role: {
        type: String,
        required: [true, 'role is required'],
        default: 'author'
    },
})

module.exports = mongoose.model('Account', AccountSchema)
