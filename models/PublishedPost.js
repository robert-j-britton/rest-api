const Post = require('./Post')

module.exports = Post.discriminator('PublishedPost',
    new mongoose.Schema({
        publishedOn: {
            type: Date,
            required: [true, 'publishedOn is required'],
            default: function() {
                return mongoose.Types.ObjectId(this._id).getTimestamp()
            }
        }
    }))
