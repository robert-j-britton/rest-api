const mongoose = require('mongoose')
const FileSchema = require('./schemas/File')

// User may have many files, reference the user to each file they create so not to exceed 16Mb doc limit
const MediaSchema = new mongoose.Schema({
    file: {
        type: FileSchema,
        require: [true, 'file is required']
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        required: [true, 'user is required']
    }
})

module.exports = mongoose.model('Media', MediaSchema)
