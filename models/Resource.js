const mongoose = require('mongoose')
const RoleAccessSchema = require('./schemas/RoleAccess')


const ResourceSchema = new mongoose.Schema({
    type: {
        type: String,
        required: [true, 'resource is required']
    },
    roleAccess: {
        type: RoleAccessSchema,
        required: [true, 'permissions is required'],
        default: {}
    }
})

module.exports = mongoose.model('Resource', ResourceSchema)
