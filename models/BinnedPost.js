const Post = require('./Post')

module.exports = Post.discriminator('BinnedPost',
    new mongoose.Schema({
        binnedOn: {
            type: Date,
            required: [true, 'binnedOn is required'],
            default: function() {
                return mongoose.Types.ObjectId(this._id).getTimestamp()
            }
        }
    })
)
