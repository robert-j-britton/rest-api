const mongoose = require('mongoose')


const CommentResponseSchema = new mongoose.Schema({
    _id: {
        type: mongoose.SchemaTypes.ObjectId,
        auto: true
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: [true, 'user is required']
    },
    posted: {
        type: Date,
        get: function() {
            return mongoose.Types.ObjectId(this._id).getTimestamp()
        }
    },
    body: {
        type: String,
        required: [true, 'body is required']
    }
})

module.exports = mongoose.model('Comment-Response', CommentResponseSchema)
