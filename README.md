## Rest API

The back-end to a CMS similar to Wordpress. Handles user account registration, authentication, user generated content such as blog-post and images. Intergrated with MongoDB and AWS.

### Setup

When forking / cloning the project run `npm install` to install the project dependencies followed by `npm start` to start the development server.

#### Overwriting existing environment variables
We use [dotenv-flow](https://www.npmjs.com/package/dotenv-flow) to manage local environment variables so don't directly add values to the `.env`, `.env.*` files, particularly private information you don't want to share on the repo (passwords etc). 

Instead create a `.env.local` and/or `.env.*.local` files which are ignored and not added to the repo.

#### Encryption Keys
The JWT token generator uses RSA key files to sign and verify tokens exchanged with the client. You should create a `public.key` and `private.key` files in the project root and store the respective keys to the files. 

We recommend [Online RSA Key Generator](http://travistidwell.com/jsencrypt/demo/) with a specified key size of `512bit`.
