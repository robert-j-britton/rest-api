exports.AUTH_FAIL_INVALID_TOKEN = "authentication failed - invalid token"
exports.AUTH_FAIL_MISSING_TOKEN = "authentication failed - missing token"
exports.ACCESS_DENIED = "access denied"
exports.USER_NOT_FOUND = "user not found"
exports.INVALID_EMAIL_PASSWORD = "authentication failed - invalid email or password"
exports.ACCESS_DENIED = "access denied"
exports.INVALID_ROLE = "invalid role"
exports.POST_NOT_FOUND = "post not found"
exports.POST_MUST_BE_PUBLISHED_TO_COMMENT = "post must be published in order to leave a comment"
exports.NOT_FOUND = "unable to find resource with the id provided"
exports.EMAIL_ALREADY_REGISTERED = "email already registered"

exports.JWT_HEADER_PAYLOAD_COOKIE = {
    name: 'auth_token',
    options: {
        maxAge:  30 * 60000,
        httpOnly: false,
        secure: process.env.cookie_secure == true,
        domain: process.env.cookie_domain,
        path: "/",
        sameSite: true
    }
}

exports.JWT_SIGNATURE_COOKIE = {
    name: 'auth_token_sig',
    options: {
        httpOnly: true,
        secure: process.env.cookie_secure == true,
        domain: process.env.cookie_domain,
        path: "/",
        sameSite: true
    }
}
