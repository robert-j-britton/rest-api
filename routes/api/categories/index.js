const categories = require('express').Router()
const PostCategoriesController = require('../../../controllers/categories')
const decodeAuthToken = require('../../../middleware/decode_auth_token')
const validateAuthToken = require('../../../middleware/validate_auth_token')
const checkAuthorisation = require('../../../middleware/check_authorisation')
const slugifyReqProps = require('../../../middleware/slugify_req_props')
const Access = require('../../../utility/Access')


categories.post('/', decodeAuthToken, validateAuthToken, checkAuthorisation(Access.create), slugifyReqProps('body.category'), PostCategoriesController.post_categories_create_one)

categories.get('/', PostCategoriesController.post_categories_get_all)

categories.patch('/:category', decodeAuthToken, validateAuthToken, checkAuthorisation(Access.update), slugifyReqProps(['body.category', 'query.category']), PostCategoriesController.post_categories_update_one)

categories.delete('/', decodeAuthToken, validateAuthToken, checkAuthorisation(Access.delete), PostCategoriesController.post_categories_delete_one)

module.exports = categories
