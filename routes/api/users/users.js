const users = require('express').Router()
const UsersController = require('../../../controllers/api/users/users')
const checkAuthorisation = require('../../../middleware/check_authorisation')
const Access = require('../../../utility/Access')
const validateObjectIdParams = require('../../../middleware/validate_objectid_params')


// Update - Update a user if the client is the target or if client has authorisation to update users other than itself.
users.patch('/user/:user_id', validateObjectIdParams, checkAuthorisation(Access.update), UsersController.users_user_update_one)

// Delete - Mark target user as inactive if the client is the target or if the client has authorisation to delete users other than itself.
users.delete('/user/:user_id', validateObjectIdParams, checkAuthorisation(Access.delete), UsersController.users_user_delete_one)


module.exports = users
