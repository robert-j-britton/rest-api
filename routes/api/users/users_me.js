const users_me = require('express').Router()
const UsersMeController = require('../../../controllers/api/users/users_me')


users_me.get('/', UsersMeController.users_me_get)

users_me.patch('/', UsersMeController.users_me_update)

users_me.delete('/', UsersMeController.users_me_delete)

module.exports = users_me
