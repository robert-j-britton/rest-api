const api = require('express').Router()
const resourceName = require('../../middleware/resource_name')
const Resource = require('../../utility/Resources')
const checkAuthentication = require('../../middleware/check_authentication')


// Public route used to register a new account and sign in.
api.use('/accounts', require('./accounts/accounts'))

// // Access to the clients personal account information.
api.use('/accounts/me', checkAuthentication, require('./accounts/accounts_me'))

// // Authentication and authorisation is decided as required by child routes.
// api.use('/users', resourceName(Resource.users), require('./users/users'))

// // Access to this route will always require authentication. as the token contains the clients' uid
// api.use('/users/me', checkAuthentication, require('./users/users_me'))


// api.use('/uploads', resourceName(Resource.uploads), require('./uploads/uploads'))

// apiRouter.use('/posts')

// apiRouter.use('/slugs')

module.exports = api
