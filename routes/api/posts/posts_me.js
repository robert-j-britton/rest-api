const posts_me = require('express').Router()
const PostsMeController = require('../../../controllers/api/posts/posts_me')

posts_me.get('/', PostsMeController.posts_me_get_all)
