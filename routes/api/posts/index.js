const posts = require('express').Router()
const noCache = require('nocache')

const decodeAuthToken = require('../../../middleware/decode_auth_token')
const validateAuthToken = require('../../../middleware/validate_auth_token')
const checkAuthorisation = require('../../../middleware/check_authorisation')
const getPostByPublication = require('../../../middleware/posts/get_post_by_publication')
const validatePost = require('../../../middleware/posts/validate_post')
const getPostBySlug = require('../../../middleware/posts/get_post_by_slug')
const getPostByCategory = require('../../../middleware/posts/get_post_by_category')
const getPostByTag = require('../../../middleware/posts/get_post_by_tag')
const getPostByAuthor = require('../../../middleware/posts/get_post_by_author')
const PostsController = require('../../../controllers/posts')
const Access = require('../../../utility/Access')


// Create new post
posts.post('/', checkAuthorisation(Access.create), validatePost, PostsController.posts_create_one)

// This route can be accessed by anyone. We validate token internally, only if user requests unpublished (draft) posts
posts.get('/', noCache(), getPostByCategory, getPostByTag, decodeAuthToken, getPostBySlug, getPostByPublication, getPostByAuthor, PostsController.posts_get)

// Update a post with the matching id
posts.patch('/:post_id', checkAuthorisation(Access.update), validatePost, PostsController.posts_update_one)

// Delete a post with matching id
posts.delete('/:post_id', checkAuthorisation(Access.delete), PostsController.posts_delete_one)


module.exports = posts
