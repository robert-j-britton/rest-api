const tags = require('express').Router()
const PostTagsController = require('../../../controllers/postTags')
const checkAuthorisation = require('../../../middleware/check_authorisation')
const slugifyReqProps = require('../../../middleware/slugify_req_props')
const Access = require('../../../utility/Access')


tags.post('/', checkAuthorisation(Access.create), slugifyReqProps('body.tag'), PostTagsController.post_tags_create_one)

tags.get('/', PostTagsController.post_tags_get_all)

tags.patch('/:tag', checkAuthorisation(Access.update), slugifyReqProps(['body.tag', 'query.tag']), PostTagsController.post_tags_update_one)

tags.delete('/', checkAuthorisation(Access.delete), PostTagsController.post_tags_delete_one)

module.exports = tags
