const accounts_me = require('express').Router()
const AccountsMeController = require('../../../controllers/api/accounts/accounts_me')
const checkAccountPassword = require('../../../middleware/check_account_password')

accounts_me.get('/', AccountsMeController.accounts_me_get)

accounts_me.patch('/', checkAccountPassword, AccountsMeController.accounts_me_update)

accounts_me.delete('/', checkAccountPassword, AccountsMeController.accounts_me_delete)

module.exports = accounts_me
