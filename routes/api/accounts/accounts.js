const accounts = require('express').Router()

const AccountsController = require('../../../controllers/api/accounts/accounts')
const validateSignIn = require('../../../middleware/accounts/validate_sign_in')
const validateRegistration = require('../../../middleware/accounts/validate_registration')
const validateAccountVerifyResend = require('../../../middleware/validate_account_verify_resend')

// Create - Save a new user document to the collection
// Publicly Accessible - Requires no authentication nor authorisation.
accounts.post('/', validateRegistration, AccountsController.accounts_register)

// Read - Get a valid token containing info about the user and flag user as active
// Publicly Accessible - Requires no authentication nor authorisation.
accounts.post('/signin', validateSignIn, AccountsController.accounts_signin)


accounts.post('/reset_password', AccountsController.accounts_reset_password)


accounts.post('/verify', AccountsController.accounts_verify)


accounts.post('/verify/resend', validateAccountVerifyResend, AccountsController.accounts_verify_resend)


module.exports = accounts
