const uploads = require('express').Router()
const UploadsController = require('../../../controllers/api/uploads/uploads')
const checkAuthorisation = require('../../../middleware/check_authorisation')
const validateUpload = require('../../../middleware/validate_upload')
const Access = require('../../../utility/Access')
const upload = require('../../../middleware/multer_upload')


uploads.post('/', checkAuthorisation(Access.create), upload.single('image'), validateUpload, UploadsController.uploads_create_one)

uploads.delete('/:media_id', checkAuthorisation(Access.delete), UploadsController.uploads_delete_one)

module.exports = uploads
