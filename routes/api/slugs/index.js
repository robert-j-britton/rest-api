const slugs = require('express').Router()
const decodeAuthToken = require('../../../middleware/decode_auth_token')
const validateAuthToken = require('../../../middleware/validate_auth_token')
const SlugController = require('../../../controllers/slug')
const slugifyReqProps = require('../../../middleware/slugify_req_props')


slugs.get('/preview', decodeAuthToken, validateAuthToken, slugifyReqProps('query.slug'), SlugController.slug_get_preview)

module.exports = slugs
