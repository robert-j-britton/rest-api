const resources = require('express').Router()
const ResourcesController = require('../../../controllers/admin/resources')
const validateResource = require('../../../middleware/resources/validate_resource')
const validateResourceUpdate = require('../../../middleware/resources/validate_resource_update')
const validateObjectIdParams = require('../../../middleware/validate_objectid_params')

resources.post('/', validateResource, ResourcesController.resources_create_one)

resources.get('/', ResourcesController.resources_get_all)

resources.patch('/resource/:resource_id', validateObjectIdParams, validateResourceUpdate, ResourcesController.resources_resource_update_one)

resources.delete('/resource/:resource_id', validateObjectIdParams, ResourcesController.resources_resource_delete_one)

module.exports = resources
