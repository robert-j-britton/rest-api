const accounts = require('express').Router()
const AccountsController = require('../../../controllers/admin/accounts')

accounts.get('/', AccountsController.accounts_get_all)

module.exports = accounts
