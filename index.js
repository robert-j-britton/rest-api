require('dotenv-flow').config({
    node_env: process.env.NODE_ENV,
    default_node_env: 'development'
})
const mongoose = require('mongoose')
const express = require('express')
const helmet = require('helmet')
const debug = require('debug')('app:main')
const debugDB = require('debug')('app:db')
const morgan = require('morgan')
const winston = require('./config/winston')
const cors = require('cors')
const cookieParser = require('cookie-parser')
const checkAdminAuthorisation = require('./middleware/check_admin_authorisation')

debug(`Running ${process.env.app_name} - ${process.env.config_id}`)

const app = express()

mongoose.connect(process.env.db_connection_string, { useNewUrlParser: true, useFindAndModify: false })
    .then(() => debugDB(`Connected to MongoDB - ${process.env.db_name}`))
    .catch(err => debugDB('Error connecting to MongoDB' + err))

mongoose.set('useCreateIndex', true)
debugDB('useCreateIndex enabled')

app.use(express.urlencoded({ extended: false }))
app.use(express.json())

app.use(function (error, req, res, next) {
    debug('client request contained invalid JSON syntax')
    if (error instanceof SyntaxError) return res.status(400).send({ error: 'Request syntax invalid'});
    next();
});
app.use(helmet())
app.use(cookieParser())

if (process.env.NODE_ENV !== 'test') app.use(morgan('combined', { stream: winston.stream }))

// Configure CORS to reject requests from any client that doesn't send Origin header and
// Any client who's Origin is not expected. (limits most Cross-Site-Requests)
// Trying to keep API requests limited to the front-end
let whitelist = []
const allowedOrigins = process.env.allowed_client_origin_urls

if (!allowedOrigins) {
  whitelist.push('*')
} else {
  whitelist = allowedOrigins.split(';')
  whitelist.forEach(origin => origin.trim())
}

// TODO: Prehaps we would like not to enable credentials for certain routes? GET requests for example.
app.use(cors(
  {
    origin: (origin, callback) => {
      if (!origin || whitelist.findIndex(url => origin === url) === -1) {
          debug('client request refused by CORS: Origin header missing')
          callback(new Error('not allowed by CORS'), false)
      } else {
          callback(null, true)
      }
    },
    credentials: true
  }
))

debug("Setup cors")

app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-auth-token");
  res.header("Access-Control-Expose-Headers", " x-auth-token");
  next();
});

debug("Configured HTTP headers")

// Enable pre-flight request check for X-Requested-With == XMLHttpRequest
app.options((req, res, next) => {
  if (req.xhr) {
      next()
  } else {
      debug('client request was not Ajax')
      res.status(400).send('bad request')
  }
})

debug("Configured Options")

// Admin route always requires client to be an admin user.
app.use('/admin', checkAdminAuthorisation, require('./routes/admin'))

debug("Configured admin routes")

// Access is decided by child routes as and when required.
app.use('/api', require('./routes/api'))

debug("Configured api routes")

const port = 3005
app.listen(port, () => debug(`Example app listening on port ${port}!`))

module.exports = app
