module.exports = {
    none: 0,
    create: 1,
    read: 2,
    update: 4,
    delete: 8
}
