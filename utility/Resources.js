module.exports = {
    users: 'users',
    posts: 'posts',
    comments: 'comments',
    responses: 'responses',
    categories: 'categories',
    uploads: 'uploads',
    tags: 'tags',
    slugs: 'slugs',
}
